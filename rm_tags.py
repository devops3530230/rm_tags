import requests
import datetime

# Configurações
gitlab_url = 'https://gitlab.com'
repository_id = 'seu_usuario/seu_repositorio'
private_token = 'seu_token_de_acesso'

# Obtém todas as tags do repositório
headers = {'PRIVATE-TOKEN': private_token}
response = requests.get(f'{gitlab_url}/api/v4/projects/{repository_id}/repository/tags', headers=headers)
tags = response.json()

# Filtra as tags antigas (neste exemplo, tags com mais de 30 dias)
threshold_date = datetime.datetime.now() - datetime.timedelta(days=30)
old_tags = [tag for tag in tags if datetime.datetime.strptime(tag['commit']['created_at'], '%Y-%m-%dT%H:%M:%S.%fZ') < threshold_date]

# Exclui as tags antigas
for tag in old_tags:
    response = requests.delete(f'{gitlab_url}/api/v4/projects/{repository_id}/repository/tags/{tag["name"]}', headers=headers)
    if response.status_code == 204:
        print(f'Tag {tag["name"]} excluída com sucesso.')
    else:
        print(f'Erro ao excluir a tag {tag["name"]}. Código de status: {response.status_code}')